# Readme

## Accessing online

- An online version of the app can be found at https://s3.eu-central-1.amazonaws.com/luke-hansford/index.html

## Running locally

- `python -m SimpleHTTPServer` (Or use a similar local server to serve the folder)
- Navigate to `http://127.0.0.1:8000/` to view the app (if your server used a different port then change out `8000` for that port).

## Building locally

- `npm install`
- `node_modules/webpack-cli/bin/webpack.js`
