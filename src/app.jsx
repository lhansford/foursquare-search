import React from 'react';

import SearchBar from './searchBar';
import ResultsList from './resultsList';

const SEARCH_QUERY = {
  client_id: 'WACX2T1ORCCXWWG2T02GQJBOKOGT4HDRQCA1Y25DOBZLO2R2',
  client_secret: 'D2RLTTRWKUYVTFQGHOSWVPK5YFF2ECVZZMUBY3XASA212ZK0',
  v: '20180325',
};

/**
 * The main component containing the state that will be shared by child components.
 */
class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      searchedFor: undefined,
      results: [],
      searchRadius: 250,
      sortBy: 'BEST_MATCH', // 'DISTANCE', 'BEST_MATCH',
    }
  }

  /**
   * Parses a Foursquare response object and updates state.
   * @param {any} responseObject The response object
   * @returns {void}
   */
  parseResponse(responseObject) {
    const response = responseObject.response;
    this.setState({
      isFetching: false,
      results: response && response.groups && response.groups.length > 0 && response.groups[0].items ?
        response.groups[0].items : [],
    });
  }

  /**
   * Sends a search query to the Foursquare API with the given parameters.
   * @param {{}} parameters A parameters object. It should contain either a 'near' or 'll' parameter
   */
  sendSearchQuery(parameters) {
    const queryParams = Object.assign({}, SEARCH_QUERY, parameters, {
      radius: this.state.searchRadius,
      limit: 50,
    });
    const url = new URL('https://api.foursquare.com/v2/venues/explore');
    Object.keys(queryParams).forEach(key => url.searchParams.append(key, queryParams[key]));
    const request = new Request(url, { method: 'GET' });
    fetch(request)
      .then(response => response.json())
      .then((response) => this.parseResponse(response))
      .catch((error) => {
        this.setState({ isFetching: false, results: [] });
      }); // TODO: Better error handling.
  }

  /**
   * Called when one of the search buttons is clicked. It will read the passed search object and
   * send a search query based on the contents.
   * @param {{ searchValue: string, coordinates: Coordinates }} searchObject An object with
   * either a search string or a JS Coordinates object to search for.
   */
  onSearchClicked(searchObject) {
    if (searchObject.searchValue && searchObject.searchValue.length > 0) {
      this.setState({ searchedFor: searchObject.searchValue, isFetching: true });
      this.sendSearchQuery({ near: searchObject.searchValue });
    } else if (searchObject.coords) {
      this.setState({ searchedFor: 'Current Location', isFetching: true });
      this.sendSearchQuery({ ll: `${searchObject.coords.latitude},${searchObject.coords.longitude}` });
    }
  }

  /**
   * Returns the current search results sorted by the appropriate method.
   * @returns {Array<{}>}
   */
  getSortedResults() {
    if (this.state.sortBy === 'DISTANCE') {
      return this.state.results
        .concat([])
        .sort((a, b) => a.venue.location.distance - b.venue.location.distance);
    }
    return this.state.results;
  }

  /**
   * Gets the appropriate results message based on the current state.
   * @returns {React.Component}
   */
  getResultsMessage() {
    if (this.state.isFetching) {
      return <p>Searching for <i>{this.state.searchedFor}</i></p>;
    } else if (!this.state.isFetching && this.state.searchedFor) {
      return <p>You searched for <i>{this.state.searchedFor}</i>. There were {this.state.results.length} results.</p>;
    } else {
      return <p>Enter something in the search bar above!</p>;
    }
  }

  render() {
    return (
      <div className="app">
        <SearchBar
          onSearchClicked={(searchObject) => this.onSearchClicked(searchObject)}
          searchRadius={this.state.searchRadius}
          setSearchRadius={searchRadius => this.setState({ searchRadius })}
        />
        <div className="results-header">
          {this.getResultsMessage()}
          <div>
            Sort By: 
            <input
              id="sort-by-best-match"
              type="radio"
              value="BEST_MATCH"
              name="sortBy"
              checked={this.state.sortBy === 'BEST_MATCH'}
              onChange={event => this.setState({ sortBy: event.target.value })}
            />
            <label for="sort-by-best-match">Best Match</label>
            <input
              id="sort-by-distance"
              type="radio"
              value="DISTANCE"
              name="sortBy"
              checked={this.state.sortBy === 'DISTANCE'}
              onChange={event => this.setState({ sortBy: event.target.value })}
            />
            <label for="sort-by-distance">Distance</label>
          </div>
        </div>
        <ResultsList results={this.getSortedResults()} />
      </div>
    );
  }
}

export default App;
