import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';

function initialiseApp() {
  ReactDOM.render(<App />, document.getElementById('app'));
};

initialiseApp();
