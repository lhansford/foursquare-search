import React from 'react';

/**
 * An individual result component.
 */
class Result extends React.Component {

  getDistanceLabel() {
    return this.props.distance !== undefined ? ` (${this.props.distance}m)` : '';
  }

  getOpenLabel() {
    if (this.props.isOpen) {
      return 'Yes';
    } else if (this.props.isOpen === false) {
      return 'No';
    }
    return 'No information available';
  }

  render() {
    return (
      <section className="result">
        <h1 className="result__title">{this.props.name}{this.getDistanceLabel()}</h1>
        <div className="result__tags">
          {this.props.categories.map(c => <div className="result__tag">{c.shortName}</div>)}
        </div>
        <div className="result__info">Recent Tip: <i>"{this.props.tip}"</i></div>
        <div className="result__info">Is it open?: {this.getOpenLabel()}</div>
        <div className="result__info">Address: <i>{this.props.location}</i></div>
      </section>
    );
  }
}

export default Result;