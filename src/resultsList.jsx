import React from 'react';

import Result from './result';

/**
 * A list of Result components derived from props.results.
 */
class ResultsList extends React.Component {

  render() {
    return (
      <div>
        {
          this.props.results.map(result =>
            <Result
              key={result.venue.id}
              name={result.venue.name}
              categories={result.venue.categories}
              location={result.venue.location.address}
              distance={result.venue.location.distance}
              tip={result.tips && result.tips.length > 0 ? result.tips[0].text : '-'}
              isOpen={result.venue.hours ? result.venue.hours.isOpen : undefined }
            />
          )
        }
      </div>
    );
  }
}

export default ResultsList;