import React from 'react';

const RADIUS_OPTIONS = [250, 500, 1000, 2500, 5000];

/**
 * A component that allows the user to "search" for a location (including a "current location"
 * shortcut).
 */
class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: 'Chinatown, Singapore', // Just a decent default value.
    };
  }

  /**
   * Sets the users current location as the location to search for venues.
   */
  searchCurrentLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => this.props.onSearchClicked({ coords: position.coords }),
      (error) => {
        // What's going on here?
        // I had a bit of trouble getting the Geolocation API working properly. I suspect this had
        // something to do with running the server locally and perhaps messing up my browser permissions. 
        // I didn't have the time to properly troubleshoot it though, so I've just added a 10 second 
        // timeout and some example co-ordinates if the API fails.
        console.log('Something went wrong, but for the sake of the example lets return some preset coords.');
        this.props.onSearchClicked({ coords: { latitude: 40.7589, longitude: -73.9851 } });
      },
      {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 0
      },
    )
  }

  render() {
    return (
      <div className="search-bar">
        <input
          id="search-input"
          type="text"
          value={this.state.searchValue}
          onChange={event => this.setState({ searchValue: event.target.value })}
        />
        <select
          value={this.props.searchRadius}
          onChange={event => this.props.setSearchRadius(event.target.value)}
        >
          {RADIUS_OPTIONS.map(value => <option value={value}>{`${value}m`}</option>)}
        </select>
        <button
          onClick={() => this.props.onSearchClicked({ searchValue: this.state.searchValue})}
        >
          Go
        </button>
        <button onClick={() => this.searchCurrentLocation()}>Search Current Location</button>
      </div>
    );
  }
}

export default SearchBar;
